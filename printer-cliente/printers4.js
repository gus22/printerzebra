
/*
*    ejecutar el proyecto desde el CMD como ADMINISTRADOR
*/
var encoding = require("encoding");
var fs = require('fs');
var os = require("os");
var nodeCmd = require('node-cmd');
const { exec } = require('child_process');
const hostname=os.hostname();

var codigo = 'ASRJP-0008224428';

// var zpl = '^XA^LH15,15^FO15,10^BY2^BCN,55,N,N,N^FD[[CODIGO]]^FS^FO100,72^A0,30,23^FD[[CODIGO]]^FS^FO285,15^A0,60,53^FD[[PRECIO]]^FS^FO25,100^A0,24,20^FD[[PRODUCTO]]^FS^XZ';
// var zpl = '^XA^FO50,50^BY3^BCN,100,Y,N,N^FD000LP-0008218957^FS^FO50,250^GB700,1,3^FS^FX Second section with recipient address and permit information.^FO50,300^BY3^BCN,100,Y,N,N^FD7501083878973^FS^XZ'
//var zpl = '^XA^LH30,35^FO15,10^BY2^BCN,55,N,N,N^FD000LP-0008218957^FS^FO100,72^A0,30,23^FD000LP008218957^FS^FO15,150^GB700,1,3^FS^FO15,200^A0,30,23^BY2^BCN,55,Y,N,N^FD7501083878973^FS^XZ'
var zpl = `^XA^LL800^LH0,0^BY2.5,3,150^A0,50,40^BY3.4,15,4^FS^FO30,200^BCN,150,Y,N,N^FD${codigo}^FS^FO00,065^A0N,080,140 ^FS^FO5,080^A0N,040,110^A0N,35,38^FS^FO10,40^A0N,35,40^FS^FO500,150^A0N,35,40^FD ^XZ`;
// var zpl = '^XA^LH15,15^FO15,10^BY2^BCN,55,N,N,N^FD000LP-0008218957^FS^FO100,72^A0,30,23^FD000LP-0008218957^FS^XZ'
// var zpl = '^XA^CI28^FO50,300^BY3^BCN,100,Y,N,N^FH^FD000LP_2d0008218957^FS^XZ'//exadecimal y utf8





// console.log(resultBuffer);
function crearArchivo(codigo){
  var zpl = `^XA^LL800^LH0,0^BY2.5,3,150^A0,50,40^BY3.4,15,4^FS^FO30,200^BCN,150,Y,N,N^FD${codigo}^FS^FO00,065^A0N,080,140 ^FS^FO5,080^A0N,040,110^A0N,35,38^FS^FO10,40^A0N,35,40^FS^FO500,150^A0N,35,40^FD ^XZ`;
  var resultBuffer = encoding.convert(zpl, 'ASCII', 'UTF-8');
  fs.writeFile('./impresion_zebra.zpl', resultBuffer, function(err) {
    if(err) {
        return console.log(err);
    }
});
}


async function excecuteTask(data){
  console.log("Llegando a:" + data);
  // resultBuffer = encoding.convert(zpl, 'ASCII', 'UTF-8');
  crearArchivo(data);
    //libera puerto NET USE LPT1 para que otro recurso lo ocupe
    await exec('net use LPT1: /delete', (err, stdout, stderr) => {
        console.log('____ delete __');
        if (err) {
          console.error(err);
          return;
        }
        console.log(stdout);
    });

    //se le indica a Windows que todo lo que llegue al puerto LPT1 lo redirija a la impresora compartida, 
    //con /PERSISTENT:YES se consigue el redireccionamiento definitivo
    await exec(`net use LPT1: \\\\${hostname}\\LaChida /persistent:yes`, (err, stdout, stderr) => {
        nodeCmd.get('net use LPT1: /delete',(err,data,stderr)=>console.log(data+"borrando configuracion..."));
        nodeCmd.get(`net use LPT1: \\\\${hostname}\\LaChida /persistent:yes`,(err,data,stderr)=>console.log("nueva configuracion"));
        //nodeCmd.get(`print ${__dirname}\\impresion_zebra.zpl `,(err,data,stderr)=>console.log("imprimiendo..."));
        console.log('____ persistent __');
        if (err) {
          console.error(err);
          return;
        }
        console.log(stdout);
    });
    
    // se manda a imprimir la etiqueta zebra a la impresora
    await exec(`print ${__dirname}\\impresion_zebra.zpl `, (err, stdout, stderr) => {
        console.log('____ print __');
        if (err) {
          console.error(err);
          return;
        }
        console.log(stdout);
    });
}

module.exports ={
  excecuteTask
}